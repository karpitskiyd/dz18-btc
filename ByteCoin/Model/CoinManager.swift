
import Foundation
protocol CoinManagerDelegate{
    func didUpdateCurrency(coin: Double)
}

struct CoinManager {
 
    var delegate: CoinManagerDelegate?
    let baseURL = "https://rest.coinapi.io/v1/exchangerate"
    let apiKey = "82F7F401-BDBE-41D7-8016-3D6C6C5BAF2B"
    
    let currencyArray = ["BTC", "ETH","BNB","USDT","SOL","ADA","XRP","USDC","LUNA","DOT","AVAX","DOGE","SHIB","MATIC","CRO","BUSD","WBTC","LTC","UNI","LINK","ALGO"]

    func getCoinPrice(for coinName: String){
    print(coinName)
        let urlString = "\(baseURL)/\(coinName)/USD?apikey=\(apiKey)"
        performRequest(urlString: urlString)
        
    }
   
    
    
    func performRequest (urlString: String){                //запрос
//       1 create url
        if let url = URL(string: urlString){
//       2 create a url session
        let session = URLSession(configuration: .default)
//       3 sesson task
        let task = session.dataTask(with: url) {(data, response, error) in
            if error != nil{
                print(error!)
                return
            }
            if let safeData = data{
                
                let btcPrice = self.parseJSON(safeData)!
                self.delegate?.didUpdateCurrency(coin: btcPrice)
            }
            
        }
            //        4 resume task
            task.resume()
    }

        
}
    func parseJSON(_ coinData: Data) ->Double? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(CoinData.self, from: coinData)
            let rate = decodedData.rate
          print(rate)
            return rate
        }
        catch{
            print(error)
            return nil
            
        }
    
    
    
    
    
    
}
}
