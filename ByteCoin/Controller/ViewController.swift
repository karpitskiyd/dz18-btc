

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, CoinManagerDelegate {
    
var coinManager = CoinManager()
   

    @IBOutlet weak var byteCoinLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var topLabel: UILabel!
    
    override func viewDidLoad() {
            super.viewDidLoad()
        // Do any additional setup after loading the view.
        coinManager.delegate = self
        pickerView.dataSource = self
        pickerView.delegate = self
        
        
        
      
        
    }

        
        
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return coinManager.currencyArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return coinManager.currencyArray[row]
    }                                                       //заполняем пикер данными из массива
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let coinName = coinManager.currencyArray[row]
        coinManager.getCoinPrice(for: coinName) 
        
    }
    func didUpdateCurrency( coin: Double){
        
        
        DispatchQueue.main.async {
            self.byteCoinLabel.text = String(format: "%.1f", coin)
        }
       
    }
            
        }
    
